package com.application.message.controller;

import com.application.message.dto.MessageRequest;
import com.application.message.message.OutboxKafkaEventHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/message")
public class MessageController {
    private final OutboxKafkaEventHelper outboxKafkaEventHelper;

    @PostMapping("/send")
    public ResponseEntity<?> sentMessage(@Valid @RequestBody MessageRequest messageRequest) {
        outboxKafkaEventHelper.sendMessage(messageRequest);
        return ResponseEntity.ok(messageRequest);
    }
}

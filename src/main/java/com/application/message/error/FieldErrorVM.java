package com.application.message.error;

import lombok.Builder;

import java.io.Serializable;

@Builder
public class FieldErrorVM implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String objectName;

    private final String field;

    private final String code;

    private final String message;

    private final String localizedMessage;

    public FieldErrorVM(String dto, String field, String code, String message, String localizedMessage) {
        this.objectName = dto;
        this.field = field;
        this.code = code;
        this.message = message;
        this.localizedMessage = localizedMessage;
    }

    public String getObjectName() {
        return objectName;
    }

    public String getField() {
        return field;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getLocalizedMessage() {
        return localizedMessage;
    }

}

package com.application.message.error;

import com.google.common.base.CaseFormat;
import lombok.val;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {
    protected final MessageSource messageSource;

    public GlobalExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(FormValidateException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ValidateMessage handleFormValidateException(FormValidateException ex, HttpServletRequest request) {
        return getValidateMessage(ex);
    }

    @ExceptionHandler(BindException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ValidateMessage handleBindException(HttpServletResponse response, BindException exception) {
        return getValidateMessage(new FormValidateException(exception.getBindingResult()));
    }

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ValidateMessage handleConstraintViolationException(ConstraintViolationException exception) {
        var exceptionMessage = new ValidateMessage();
        Map<String, Object> errors = new HashMap<>();
        exceptionMessage.setStatus(HttpStatus.BAD_REQUEST.value());


        var constraintViolations = exception.getConstraintViolations();
        for (val violation : constraintViolations) {
            errors.put(violation.getPropertyPath().toString(), violation.getMessage());
        }

        exceptionMessage.setErrors(errors);
        return exceptionMessage;
    }

    private ValidateMessage getValidateMessage(FormValidateException ex) {
        var exceptionMessage = new ValidateMessage();
        Map<String, Object> errors = new HashMap<>();
        exceptionMessage.setStatus(HttpStatus.BAD_REQUEST.value());
        if (ex.getBindingResult() != null) {
            for (var error : ex.getBindingResult().getFieldErrors()) {
                putFieldError(errors, error);
            }
        }
        if (ex.getMessageResult() != null) {
            for (val error : ex.getMessageResult().entrySet()) {
                val key = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, error.getKey());
                if (!errors.containsKey(key)) {
                    try {
                        val keyLanguage = String.format("%s.%s", ex.getStackTrace()[0].getClassName(), key);
                        errors.put(key, messageSource.getMessage(keyLanguage, null, LocaleContextHolder.getLocale()));
                    } catch (NoSuchMessageException ns) {
                        val value = error.getValue();
                        errors.put(key, value);
                    }
                }
            }
        }
        if (ex.getParams() != null) {
            for (val param : ex.getParams().entrySet()) {
                val key = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, param.getKey());
                try {
                    val keyLanguage = String.format("%s.%s", ex.getStackTrace()[0].getClassName(), key);
                    if (!errors.containsKey(key)) {
                        errors.put(key, messageSource.getMessage(keyLanguage, param.getValue(), LocaleContextHolder.getLocale()));
                    }
                } catch (NoSuchMessageException ns) {
                    errors.put(key, ex.getParams().get(key));
                }
            }
        }
        exceptionMessage.setErrors(errors);
        return exceptionMessage;
    }
    private void putFieldError(Map<String, Object> errors, FieldError error) {
        try {
            val key = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, error.getDefaultMessage());
            val keyLanguage = String.format("%s.%s", error.getObjectName(), key);
            errors.put(key, messageSource.getMessage(keyLanguage, null, LocaleContextHolder.getLocale()));
        } catch (NoSuchMessageException | NullPointerException ex) {
            val key = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, error.getField());
            val value = error.getDefaultMessage();
            if (!errors.containsKey(key)) {
                errors.put(key, value);
            }
        }
    }
}
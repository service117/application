package com.application.message.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.Map;
import java.util.Set;

@JsonRootName("data_error")
public class ValidateMessage {
    private int status;
    private Map<String, Object> errors;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<Integer> errorCodes;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Map<String, Object> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, Object> errors) {
        this.errors = errors;
    }

    public Set<Integer> getErrorCodes() {
        return errorCodes;
    }

    public void setErrorCodes(Set<Integer> errorCodes) {
        this.errorCodes = errorCodes;
    }
}

package com.application.message.error;

import java.net.URI;

public final class ErrorConstants {
    public static final String PROBLEM_BASE_URL = "http://localhost:8088";
    public static final URI CONSTRAINT_VIOLATION_TYPE = URI.create(PROBLEM_BASE_URL + "/constraint-violation");
    public static final String ERR_VALIDATION = "error.validation";
}

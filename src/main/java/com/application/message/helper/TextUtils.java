package com.application.message.helper;

import com.google.common.base.CaseFormat;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtils {
    private TextUtils() {
    }

    public static String generatePropertyCode(String input) {
        if (StringUtils.isBlank(input)) {
            return "";
        } else {
            String code = removeVietnameseChar(input);
            code = replaceSpecialWithUnderScore(code);
            return StringUtils.upperCase(code);
        }
    }

    private static String replaceSpecialWithUnderScore(String code) {
        if (StringUtils.isBlank(code)) {
            return code;
        } else if (Pattern.compile("[^A-Za-z0-9_]").matcher(code).find()) {
            return replaceSpecialWithUnderScore(code.replaceAll("[^A-Za-z0-9_]", "_"));
        } else {
            return Pattern.compile("_{2}").matcher(code).find() ? replaceSpecialWithUnderScore(code.replaceAll("_+", "_")) : code;
        }
    }

    public static String removeVietnameseChar(String input) {
        if(StringUtils.isBlank(input)) return "";
        input = input.replaceAll("[àáạảãâầấậẩẫăằắặẳẵ]", "a");
        input = input.replaceAll("[èéẹẻẽêềếệểễ]", "e");
        input = input.replaceAll("[ìíịỉĩ]", "i");
        input = input.replaceAll("[òóọỏõôồốộổỗơờớợởỡ]", "o");
        input = input.replaceAll("[ùúụủũưừứựửữ]", "u");
        input = input.replaceAll("[ỳýỵỷỹ]", "y");
        input = input.replaceAll("[đ]", "d");
        input = input.replaceAll("[ÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴ]", "A");
        input = input.replaceAll("[ÈÉẸẺẼÊỀẾỆỂỄ]", "E");
        input = input.replaceAll("[ÌÍỊỈĨ]", "I");
        input = input.replaceAll("[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]", "O");
        input = input.replaceAll("[ÙÚỤỦŨƯỪỨỰỬỮ]", "U");
        input = input.replaceAll("[ỲÝỴỶỸ]", "Y");
        input = input.replaceAll("[Đ]", "D");
        return input;
    }

    public static String toSnakeCase(String input) {
        return StringUtils.isEmpty(input) ? input : CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, input);
    }

    public static String removeSpecialCharacters(String input){
        if(StringUtils.isBlank(input)) return "";
        return removeVietnameseChar(input).replaceAll("[^A-Za-z0-9 ]", "");
    }
    public static String getStringByRegex(String regex,String str){
        if(StringUtils.isNotBlank(str)){
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(str);
            while (m.find()) {
                return m.group();
            }
        }
        return "";
    }
}

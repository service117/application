package com.application.message.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "outbox_kafka_events")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OutboxKafkaEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int DEFAULT_VERSION = 0;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "aggregate_type", nullable = false)
    private String aggregateType;

    @NotNull
    @Size(max = 255)
    @Column(name = "event_type", nullable = false)
    private String eventType;

    @NotNull
    @Column(name = "payload", nullable = false)
    private String payload;

    @NotNull
    @Size(max = 255)
    @Column(name = "actor", nullable = false)
    private String actor;

    @NotNull
    @CreatedDate
    @Column(name = "created_date", nullable = false, updatable = false)
    private Instant createdDate;

    @Column(name = "version", nullable = false)
    private int version;

}

package com.application.message.dto;

import com.application.message.enumerate.MessageProvider;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@JsonRootName("message")
public class MessageRequest {
    @Enumerated(EnumType.STRING)
    private MessageProvider provider;
    @NotBlank
    @Size(max = 255)
    private String topic;
    @Size(max = 255)
    private String routingKey;
    @NotBlank
    @Size(max = 255)
    private String eventType;
    @NotBlank
    private String payload;
    private boolean isSave = true;
}

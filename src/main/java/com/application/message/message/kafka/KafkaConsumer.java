package com.application.message.message.kafka;

import com.application.message.config.Constants;
import com.application.message.message.SimpleOutboxEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor
public class KafkaConsumer {

    @KafkaListener(topics = Constants.CUSTOMER_HANDLER,groupId = "CUSTOMER_1", concurrency = "5")
    public void listener(@Headers Map<String, Object> headers, @Payload SimpleOutboxEvent outboxEvent) {
        log.info("header event",headers.toString());
        log.info(outboxEvent.getPayload());
    }
}

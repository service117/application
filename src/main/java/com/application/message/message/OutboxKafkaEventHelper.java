package com.application.message.message;

import com.application.message.repository.OutboxKafkaEventRepository;
import com.application.message.config.Constants;
import com.application.message.dto.MessageRequest;
import com.application.message.error.FormValidateException;
import com.application.message.model.OutboxKafkaEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Slf4j
@Component
@RequiredArgsConstructor
public class OutboxKafkaEventHelper {
    private final OutboxKafkaEventRepository outboxKafkaEventRepository;
    private final KafkaTemplate kafkaTemplate;

    @Transactional
    public void sendMessage(MessageRequest messageRequest) {
        switch (messageRequest.getProvider()){
            case KAFKA:
                kafka(messageRequest);
                return;
            case RABBIT:
                rabbit(messageRequest);
                return;
            default:
                throw new FormValidateException("error","Hệ thống chưa hỗ trợ");
        }

    }

    @Transactional
    public void kafka(MessageRequest messageRequest) {
        if (messageRequest.isSave()) {
            kafka(messageRequest.getTopic(), messageRequest.getEventType(), messageRequest.getPayload());
        } else {
            String aggregateType = messageRequest.getPayload().concat(Constants.TOPIC_PREFIX);
            kafka(aggregateType, messageRequest.getPayload());
        }

    }

    @Transactional
    public void kafka(String aggregateType, String eventType, String payload) {
        var outboxEvent = OutboxKafkaEvent.builder()
                .actor(Constants.SYSTEM)
                .aggregateType(aggregateType)
                .eventType(eventType)
                .payload(payload)
                .version(OutboxKafkaEvent.DEFAULT_VERSION)
                .createdDate(Instant.now())
                .build();
        outboxKafkaEventRepository.save(outboxEvent);
    }

    public void kafka(String topic, String payload) {
        kafkaTemplate.send(topic, payload);
    }

    public void rabbit(MessageRequest messageRequest){

    }
}

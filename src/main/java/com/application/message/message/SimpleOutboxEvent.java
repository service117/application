package com.application.message.message;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class SimpleOutboxEvent implements Serializable {
    private String payload;
    private String eventType;
}

package com.application.message.message.rabbit;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

//
//@Component
@Slf4j
public class RabbitConsumer {

    @RabbitListener(queues = "test_queue")
    public void test(String message){
        log.info("message 1: {}",message);
    }
    @RabbitListener(queues = "test_queue2")
    public void test2(String message){
        log.info("message 2:{}",message);
    }

    @RabbitListener(queues = "test_queue3")
    public void test3(String message){
        log.info("message 2:{}",message);
    }
}

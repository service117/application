package com.application.message.repository;

import com.application.message.model.OutboxKafkaEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OutboxKafkaEventRepository extends JpaRepository<OutboxKafkaEvent, Long> {
}

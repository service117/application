package com.application.message.config;

public class Constants {
    public static final String SYSTEM = "system";
//    TOPIC KAFKA
    public static final String TOPIC_PREFIX = "ev.";
    public static final String TOPIC_CUSTOMER = "CUSTOMER";
    public static final String CUSTOMER_HANDLER = "ev.CUSTOMER";
}

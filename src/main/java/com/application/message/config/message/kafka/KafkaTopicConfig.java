package com.application.message.config.message.kafka;

import com.application.message.config.Constants;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class KafkaTopicConfig {
    @Value("#{'${spring.kafka.servers}'.split(',')}")
    private List<String> servers;

    @Bean
    public NewTopic topic1() {
        return TopicBuilder.name(StringUtils.join(Constants.TOPIC_PREFIX,Constants.TOPIC_CUSTOMER))
                .partitions(3)
                .build();
    }

    @Bean
    KafkaAdmin admin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
        return new KafkaAdmin(configs);
    }
}

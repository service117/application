package com.application.message.config.message.rabbit;

import com.rabbitmq.client.AMQP;
import org.springframework.amqp.core.*;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.util.CollectionUtils;

import java.util.*;

//@Configuration
//@EnableRabbit
public class RabbitConfig {
    @Bean
    @Primary
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory, MessageConverter messageConverter) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter);
        return rabbitTemplate;
    }

    @Bean
    @Primary
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    @Primary
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory, MessageConverter messageConverter) {
        var factory = new SimpleRabbitListenerContainerFactory();
        factory.setMessageConverter(messageConverter);
        factory.setConnectionFactory(connectionFactory);
        return factory;
    }

    @Bean
    public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
        var ra = new RabbitAdmin(connectionFactory);
        declareFanoutExchange(ra, Arrays.asList("test_queue", "test_queue2"), "FanoutExchange");
        declareTopicExchange(ra, "test_queue3", "topicExchange", "testTopic");
        return ra;
    }

    public void declareTopicExchange(RabbitAdmin rabbitAdmin, String queueNames, String exchangeName, String routingKey) {
        Map<String, Object> arguments = new HashMap<>();
        var topicExchange = new TopicExchange(exchangeName, true, true);
        rabbitAdmin.declareExchange(topicExchange);
        var queue = new Queue(queueNames, false, false, false, arguments);
        rabbitAdmin.declareQueue(queue);
        Binding binding = BindingBuilder.bind(queue).to(topicExchange).with(routingKey);
        rabbitAdmin.declareBinding(binding);
    }

    public void declareFanoutExchange(RabbitAdmin rabbitAdmin, List<String> queueNames, String exchangeName) {
        if (CollectionUtils.isEmpty(queueNames))
            return;
        Map<String, Object> arguments = new HashMap<>();
        var fanoutExchange = new FanoutExchange(exchangeName, true, true);
        rabbitAdmin.declareExchange(fanoutExchange);
        for (String queueName : queueNames) {
            var queue = new Queue(queueName, false, false, false, arguments);
            rabbitAdmin.declareQueue(queue);
            Binding binding = BindingBuilder.bind(queue).to(fanoutExchange);
            rabbitAdmin.declareBinding(binding);
        }
    }

}

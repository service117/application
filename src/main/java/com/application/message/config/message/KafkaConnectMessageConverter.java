package com.application.message.config.message;

import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.SchemaAndValue;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.json.JsonConverter;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.converter.AbstractMessageConverter;
import org.springframework.util.MimeType;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.*;

public abstract class KafkaConnectMessageConverter extends AbstractMessageConverter {
    private static final MimeType KAFKA_CONNECT_JSON = new MimeType("application", "connect-json");
    private final JsonConverter jsonConverter;

    public KafkaConnectMessageConverter() {
        super(KAFKA_CONNECT_JSON);
        jsonConverter = new JsonConverter();
        Map<String, Object> configs = new HashMap<>();
        configs.put("schemas.enable", "true");
        jsonConverter.configure(configs, false);
    }

    @Override
    protected boolean supports(Class<?> aClass) {
        try {
            return aClass.getConstructor() != null;
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    @Override
    protected Object convertFromInternal(Message<?> message, Class<?> targetClass, Object conversionHint) {
        byte[] payload = (byte[]) message.getPayload();
        String topic = message.getHeaders().getOrDefault(KafkaHeaders.RECEIVED_TOPIC, "").toString();
        SchemaAndValue schemaAndValue = jsonConverter.toConnectData(topic, payload);
        try {
            return convertToObject((Struct) schemaAndValue.value(), targetClass);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected <T> T convertToObject(Struct struct, Class<T> clazz) throws NoSuchMethodException, SecurityException,
        InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Constructor<T> ctor = clazz.getConstructor();
        T object = ctor.newInstance();
        List<Field> fields = getAllFields(clazz);
        for (Field field : fields) {
            field.setAccessible(true);
            String fieldName = field.getName();
            fieldName = convertCase(fieldName);
            org.apache.kafka.connect.data.Field schemeField = struct.schema().field(fieldName);
            if (schemeField != null) {
                Object value = struct.get(schemeField);
                if (field.getType().equals(boolean.class) || field.getType().equals(Boolean.class)) {
                    if (value != null) {
                        field.set(object, !"0".equals(value.toString()));
                    }
                } else if (field.getType().equals(UUID.class) && schemeField.schema().type().equals(Schema.Type.STRING)) {
                    if (value != null) {
                        field.set(object, UUID.fromString(value.toString()));
                    }
                } else if (field.getType().equals(Instant.class) && schemeField.schema().type().equals(Schema.Type.INT64)) {
                    if (value != null) {
                        field.set(object, Instant.ofEpochMilli((long) value));
                    }
                } else {
                    field.set(object, value);
                }
            }
        }
        return object;
    }

    private static List<Field> getAllFields(Class<?> clazz) {
        return getAllFields(new ArrayList<>(), clazz);
    }

    private static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));
        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        return fields;
    }

    protected abstract String convertCase(String fieldName);
}

package com.application.message.config.message.kafka;

import com.fasterxml.jackson.databind.JsonNode;
import com.application.message.helper.JsonHelper;
import com.application.message.message.SimpleOutboxEvent;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.*;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.converter.MessagingMessageConverter;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@Slf4j
@EnableKafka
@RequiredArgsConstructor
public class KafkaConfig {
    @Value("#{'${spring.kafka.servers}'.split(',')}")
    private  List<String> bootstrapServers;
    private final JsonHelper jsonHelper;


    @Bean
    public Map<String, Object> producerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class);
        props.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 70 * 1000);
        return props;
    }
    @Bean
    @Primary
    public ProducerFactory<String, String> producerFactory() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());

    }
    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class);
        return props;
    }
    @Bean
    @Primary
    public ConsumerFactory<String, SimpleOutboxEvent> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs());
    }

    @Bean
    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, SimpleOutboxEvent>> kafkaListenerContainerFactory() {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, SimpleOutboxEvent>();
        factory.setConsumerFactory(consumerFactory());
        factory.setMessageConverter(new CustomToStringMessageConverter());
        return factory;
    }
    public class CustomToStringMessageConverter extends MessagingMessageConverter {
        @SneakyThrows
        @Override
        protected SimpleOutboxEvent extractAndConvertValue(ConsumerRecord<?, ?> record, Type type){
            String value = (String) record.value();
            JsonNode jsonNode = jsonHelper.getJsonNode(value,"payload");
            SimpleOutboxEvent outboxEvent = new SimpleOutboxEvent();
            if(jsonNode !=null) {
                outboxEvent.setPayload(jsonNode.toString());
            } else {
                outboxEvent.setPayload(value);
            }
            return outboxEvent;
        }
    }
}
